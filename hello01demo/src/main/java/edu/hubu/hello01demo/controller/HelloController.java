package edu.hubu.hello01demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.lang.String;

@Controller
public class HelloController {
    @ResponseBody
    @GetMapping("/hello")
    public static String  hello(){
        return "Hello World";
    }
}
